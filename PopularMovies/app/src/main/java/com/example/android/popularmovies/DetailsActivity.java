package com.example.android.popularmovies;

import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import android.os.AsyncTask;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.View;

import com.example.android.popularmovies.database.FavoritesDatabase;
import com.example.android.popularmovies.databinding.ActivityDetailsBinding;
import com.example.android.popularmovies.utilities.JsonUtilities;
import com.example.android.popularmovies.utilities.NetworkUtilities;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {
    private Movie mMovie;
    private ArrayList<Trailer> mTrailers;
    private ArrayList<Review> mReviews;

    private ActivityDetailsBinding mBinding;

    private FavoritesDatabase mDb;
    private boolean mIsFavorite;

    private static final String INPUT_DATE_FORMAT = "yyyy-MM-dd";
    private static final String OUTPUT_DATE_FORMAT = "MMMM dd, yyyy";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_details);
        mDb = FavoritesDatabase.getInstance(this);
        setupFavoritesViewModel();

        mMovie = new Movie(getIntent().getIntExtra(JsonUtilities.ID, 0),
                getIntent().getStringExtra(JsonUtilities.TITLE),
                getIntent().getStringExtra(JsonUtilities.RELEASE_DATE),
                getIntent().getStringExtra(JsonUtilities.POSTER_IMAGE_PATH),
                getIntent().getStringExtra(JsonUtilities.VOTE_AVG),
                getIntent().getStringExtra(JsonUtilities.PLOT_SYNOPSIS));

        getSupportActionBar().setTitle(mMovie.getTitle());

        mBinding.tvReleaseDate.setText(getString(R.string.release_date) + parseDate());

        mBinding.tvVoteAvg.setText(getString(R.string.vote_average) + mMovie.getVoteAvg() +  getString(R.string.vote_total));

        mBinding.tvPlotSummary.setText(mMovie.getPlotSynopsis());

        mBinding.favoriteButton.setOnClickListener(this);

        Picasso.get()
                .load(NetworkUtilities.buildPosterImageUrl(mMovie.getImgSrc()))
                .error(android.R.drawable.ic_delete)
                .into(mBinding.ivPoster);

        new FetchTrailersTask().execute(mMovie.getId());
        new FetchReviewsTask().execute(mMovie.getId());
    }

    private void addToFavoritesDatabase() {
        Thread dbThread = new Thread(new Runnable() {
            @Override
            public void run() {
                mDb.movieDao().insertMovie(mMovie);
            }
        });
        dbThread.start();
    }

    private void removeFromFavoritesDatabase() {
        Thread dbThread = new Thread(new Runnable() {
            @Override
            public void run() {
                mDb.movieDao().deleteMovie(mMovie);
            }
        });
        dbThread.start();
    }

    private void setupFavoritesViewModel() {
        FavoritesViewModel viewModel = ViewModelProviders.of(this).get(FavoritesViewModel.class);
        viewModel.getFavoriteMovies().observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(@Nullable List<Movie> movies) {
                for(int i = 0; i < movies.size(); i++) {
                    if (mMovie.getId() == movies.get(i).getId()) {
                        updateFavorite(true);
                        return;
                    }
                }
                updateFavorite(false);
            }
        });
    }

    private void updateFavorite(boolean isFavorite) {
        mIsFavorite = isFavorite;
        mBinding.favoriteButton.setChecked(mIsFavorite);
    }

    private String parseDate() {
        String parsedDate = mMovie.getReleaseDate();

        try {
            Date releaseDate = new SimpleDateFormat(INPUT_DATE_FORMAT).parse(mMovie.getReleaseDate());
            parsedDate = new SimpleDateFormat(OUTPUT_DATE_FORMAT).format(releaseDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return parsedDate;
    }

    private void updateUI() {

        if (mTrailers != null) {
            if(mTrailers.size() >= 1) {
                mBinding.tvTrailer1.setText(mTrailers.get(0).getName());
                mBinding.tvTrailer1.setVisibility(View.VISIBLE);
                mBinding.tvTrailer1.setOnClickListener(this);
            }
            if(mTrailers.size() >= 2) {
                mBinding.tvTrailer2.setText(mTrailers.get(1).getName());
                mBinding.tvTrailer2.setVisibility(View.VISIBLE);
                mBinding.tvTrailer2.setOnClickListener(this);
            }
            if(mTrailers.size() >= 3) {
                mBinding.tvTrailer3.setText(mTrailers.get(2).getName());
                mBinding.tvTrailer3.setVisibility(View.VISIBLE);
                mBinding.tvTrailer3.setOnClickListener(this);
            }
        }

        if(mReviews != null) {
            if (mReviews.size() >= 1) {
                mBinding.tvReview1.setText(mReviews.get(0).getAuthor());
                mBinding.tvReview1.setVisibility(View.VISIBLE);
                mBinding.tvReview1.setOnClickListener(this);
            }
            if (mReviews.size() >= 2) {
                mBinding.tvReview2.setText(mReviews.get(1).getAuthor());
                mBinding.tvReview2.setVisibility(View.VISIBLE);
                mBinding.tvReview2.setOnClickListener(this);
            }
            if (mReviews.size() >= 3) {
                mBinding.tvReview3.setText(mReviews.get(2).getAuthor());
                mBinding.tvReview3.setVisibility(View.VISIBLE);
                mBinding.tvReview3.setOnClickListener(this);
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.tv_trailer1:
                startYoutubeTrailer(mTrailers.get(0).getKey());
                break;
            case R.id.tv_trailer2:
                startYoutubeTrailer(mTrailers.get(1).getKey());
                break;
            case R.id.tv_trailer3:
                startYoutubeTrailer(mTrailers.get(2).getKey());
                break;
            case R.id.tv_review1:
                startReviewFragment(mReviews.get(0));
                break;
            case R.id.tv_review2:
                startReviewFragment(mReviews.get(1));
                break;
            case R.id.tv_review3:
                startReviewFragment(mReviews.get(2));
                break;
            case R.id.favorite_button:
                if(mIsFavorite) {
                    removeFromFavoritesDatabase();
                } else {
                    addToFavoritesDatabase();
                }
                break;
        }

    }

    private void startYoutubeTrailer(String key) {
        Intent trailerIntent = new Intent(Intent.ACTION_VIEW,
                NetworkUtilities.buildYoutubeUri(key));
        startActivity(trailerIntent);
    }

    private void startReviewFragment(Review review) {
        Fragment newFragment = ReviewFragment.newInstance(review.getAuthor(), review.getContent());
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.review_fragment_container, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();
    }

    class FetchTrailersTask extends AsyncTask<Integer, Void, ArrayList<Trailer>> {
        @Override
        protected ArrayList<Trailer> doInBackground(Integer... ids) {
            if (ids[0] == 0) {
                return null;
            }

            ArrayList<Trailer> trailers;
            try {
                String jsonResponse = NetworkUtilities.getResponseFromHttpUrl(
                        NetworkUtilities.buildTrailersUrl(ids[0]));
                trailers = JsonUtilities.parseTrailers(jsonResponse);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            return trailers;
        }

        @Override
        protected void onPostExecute(ArrayList<Trailer> trailers) {
            if(trailers  != null) {
                mTrailers = trailers;
                updateUI();
            }
        }
    }

    class FetchReviewsTask extends AsyncTask<Integer, Void, ArrayList<Review>> {
        @Override
        protected ArrayList<Review> doInBackground(Integer... ids) {
            if (ids[0] == 0) {
                return null;
            }

            ArrayList<Review> reviews;
            try {
                String jsonResponse = NetworkUtilities.getResponseFromHttpUrl(
                        NetworkUtilities.buildReviewsUrl(ids[0]));
                reviews = JsonUtilities.parseReviews(jsonResponse);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            return reviews;
        }

        @Override
        protected void onPostExecute(ArrayList<Review> reviews) {
            if(reviews  != null) {
                mReviews = reviews;
                updateUI();
            }
        }
    }
}
