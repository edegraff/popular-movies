package com.example.android.popularmovies.utilities;

import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class NetworkUtilities {

    private static final String API_KEY = "87bd57f41684458218937ea05c28b290";
    private static final String API_KEY_PARAM = "api_key";
    private static final String MOVIE_DB_URL = "http://api.themoviedb.org/3/movie/";
    private static final String POSTER_IMG_URL = "http://image.tmdb.org/t/p/";
    private static final String POSTER_IMG_SIZE = "w342";
    private static final String TRAILERS_PATH = "videos";
    private static final String REVIEWS_PATH = "reviews";
    private static final String YOUTUBE_URL = "https://www.youtube.com/watch";
    private static final String YOUTUBE_VIDEO_PARAM = "v";


    public static URL buildMoviesUrl(String queryType) {
        Uri uri = Uri.parse(MOVIE_DB_URL).buildUpon()
                .appendPath(queryType)
                .appendQueryParameter(API_KEY_PARAM, API_KEY)
                .build();

        URL url = null;
        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;
    }

    public static URL buildTrailersUrl(int id) {
        return buildGetMovieInfoUrl(id, TRAILERS_PATH);
    }

    public static URL buildReviewsUrl(int id) {
        return buildGetMovieInfoUrl(id, REVIEWS_PATH);
    }

    private static URL buildGetMovieInfoUrl(int id, String infoType) {
        Uri uri = Uri.parse(MOVIE_DB_URL).buildUpon()
                .appendPath(String.valueOf(id))
                .appendPath(infoType)
                .appendQueryParameter(API_KEY_PARAM, API_KEY)
                .build();

        URL url = null;
        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;
    }

    public static String buildPosterImageUrl(String posterPath) {
        Uri uri = Uri.parse(POSTER_IMG_URL).buildUpon()
                .appendPath(POSTER_IMG_SIZE)
                .appendPath(posterPath.replace("/", ""))
                .build();

        return uri.toString();
    }

    public static Uri buildYoutubeUri(String key) {
        return Uri.parse(YOUTUBE_URL).buildUpon()
                .appendQueryParameter(YOUTUBE_VIDEO_PARAM, key)
                .build();
    }

    // getResponseFromHttpUrl method copied from Sunshine project
    public static String getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }
}
