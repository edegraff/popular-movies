package com.example.android.popularmovies;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Movie {
    @PrimaryKey
    private int id;
    private String title;
    private String releaseDate;
    private String imgSrc;
    private String voteAvg;
    private String plotSynopsis;

    public Movie(int id, String title, String releaseDate, String imgSrc, String voteAvg, String plotSynopsis) {
        this.id = id;
        this.title = title;
        this.releaseDate = releaseDate;
        this.imgSrc = imgSrc;
        this.voteAvg = voteAvg;
        this.plotSynopsis = plotSynopsis;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getImgSrc() {
        return imgSrc;
    }


    public String getVoteAvg() {
        return voteAvg;
    }

    public String getPlotSynopsis() {
        return plotSynopsis;
    }
}
