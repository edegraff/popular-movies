package com.example.android.popularmovies;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.popularmovies.databinding.FragmentReviewBinding;

public class ReviewFragment extends Fragment {
    private static final String ARG_REVIEWER = "param1";
    private static final String ARG_CONTENT = "param2";

    private String mReviewer;
    private String mContent;
    private FragmentReviewBinding mBinding;

    public ReviewFragment() {
        // Required empty public constructor
    }

    public static ReviewFragment newInstance(String reviewer, String content) {
        ReviewFragment fragment = new ReviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_REVIEWER, reviewer);
        args.putString(ARG_CONTENT, content);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mReviewer = getArguments().getString(ARG_REVIEWER);
            mContent = getArguments().getString(ARG_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_review, container, false);
        mBinding.tvReviewTitle.setText(mReviewer);
        mBinding.tvReviewContent.setText(mContent);

        return mBinding.getRoot();
    }

}
