package com.example.android.popularmovies;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.example.android.popularmovies.database.FavoritesDatabase;

import java.util.List;

public class FavoritesViewModel extends AndroidViewModel {
    private LiveData<List<Movie>> favoriteMovies;

    public FavoritesViewModel(@NonNull Application application) {
        super(application);
        FavoritesDatabase database = FavoritesDatabase.getInstance(this.getApplication());
        favoriteMovies = database.movieDao().loadAllMovies();
    }

    public LiveData<List<Movie>> getFavoriteMovies() {
        return favoriteMovies;
    }
}
