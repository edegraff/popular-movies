package com.example.android.popularmovies;

        import androidx.lifecycle.Observer;
        import androidx.lifecycle.ViewModelProviders;
        import android.content.Intent;
        import android.os.AsyncTask;
        import androidx.appcompat.app.AppCompatActivity;
        import android.os.Bundle;
        import androidx.recyclerview.widget.GridLayoutManager;
        import androidx.recyclerview.widget.RecyclerView;
        import android.util.Log;
        import android.view.Menu;
        import android.view.MenuInflater;
        import android.view.MenuItem;

        import androidx.annotation.Nullable;

        import com.example.android.popularmovies.utilities.JsonUtilities;
        import com.example.android.popularmovies.utilities.NetworkUtilities;

        import java.net.URL;
        import java.util.ArrayList;
        import java.util.List;

public class MainActivity extends AppCompatActivity implements MovieAdapter.MovieAdapterOnClickHandler {
    private static final int SPAN_COUNT = 2;
    private static final int POPULAR_VIEW = 0;
    private static final int TOP_RATED_VIEW = 1;
    private static final int FAVORITES_VIEW = 2;
    private static final String CURRENT_VIEW = "current-view";

    private RecyclerView mRecyclerView;
    private MovieAdapter mMovieAdapter;
    private ArrayList<Movie> mFavoriteMovies;

    private int currentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GridLayoutManager layoutManager = new GridLayoutManager(this, SPAN_COUNT);

        mMovieAdapter = new MovieAdapter(this);

        mRecyclerView = findViewById(R.id.recyclerview_movies);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mMovieAdapter);

        setupFavoritesViewModel();
        if(savedInstanceState != null && savedInstanceState.containsKey(CURRENT_VIEW)) {
            currentView = savedInstanceState.getInt(CURRENT_VIEW);
            switch (currentView) {
                case POPULAR_VIEW:
                    fetchPopularMovies();
                    break;
                case TOP_RATED_VIEW:
                    fetchTopRatedMovies();
                    break;
                case FAVORITES_VIEW:
                    mMovieAdapter.setMovieData(mFavoriteMovies);
            }
        } else {
            fetchPopularMovies();
            currentView = POPULAR_VIEW;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.movie_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_popular) {
            fetchPopularMovies();
            currentView = POPULAR_VIEW;
            return true;
        }

        if(id == R.id.action_top_rated) {
            fetchTopRatedMovies();
            currentView = TOP_RATED_VIEW;
            return true;
        }

        if(id == R.id.action_favorites) {
            mMovieAdapter.setMovieData(mFavoriteMovies);
            currentView = FAVORITES_VIEW;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CURRENT_VIEW, currentView);
    }

    private void fetchPopularMovies() {
        new FetchMoviesTask().execute(NetworkUtilities.buildMoviesUrl("popular"));
    }

    private void fetchTopRatedMovies() {
        new FetchMoviesTask().execute(NetworkUtilities.buildMoviesUrl("top_rated"));
    }

    class FetchMoviesTask extends AsyncTask<URL, Void, ArrayList<Movie>> {
        @Override
        protected ArrayList<Movie> doInBackground(URL... urls) {
            if (urls.length == 0) {
                return null;
            }

            ArrayList<Movie> movies;
            try {
                String jsonResponse = NetworkUtilities.getResponseFromHttpUrl(urls[0]);
                Log.v("MOVIEDB", jsonResponse);
                movies = JsonUtilities.parseMovies(jsonResponse);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            return movies;
        }

        @Override
        protected void onPostExecute(ArrayList<Movie> movies) {
            if(movies  != null) {
                mMovieAdapter.setMovieData(movies);
            }
        }
    }

    @Override
    public void onClick(Movie movie) {
        Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);
        intent.putExtra(JsonUtilities.ID, movie.getId());
        intent.putExtra(JsonUtilities.TITLE, movie.getTitle());
        intent.putExtra(JsonUtilities.RELEASE_DATE, movie.getReleaseDate());
        intent.putExtra(JsonUtilities.POSTER_IMAGE_PATH, movie.getImgSrc());
        intent.putExtra(JsonUtilities.VOTE_AVG, movie.getVoteAvg());
        intent.putExtra(JsonUtilities.PLOT_SYNOPSIS, movie.getPlotSynopsis());

        startActivity(intent);
    }

    private void setupFavoritesViewModel() {
        FavoritesViewModel viewModel = ViewModelProviders.of(this).get(FavoritesViewModel.class);
        viewModel.getFavoriteMovies().observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(@Nullable List<Movie> movies) {
                mFavoriteMovies = new ArrayList<Movie>(movies);
                if(currentView == FAVORITES_VIEW) {
                    mMovieAdapter.setMovieData(mFavoriteMovies);
                }
            }
        });
    }
}
