package com.example.android.popularmovies.utilities;

import com.example.android.popularmovies.Movie;
import com.example.android.popularmovies.Review;
import com.example.android.popularmovies.Trailer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JsonUtilities {
    private static final String MOVIE_RESULTS = "results";
    public static final String TITLE = "title";
    public static final String RELEASE_DATE = "release_date";
    public static final String POSTER_IMAGE_PATH = "poster_path";
    public static final String VOTE_AVG = "vote_average";
    public static final String PLOT_SYNOPSIS = "overview";
    public static final String ID = "id";
    public static final String REVIEW_AUTHOR = "author";
    public static final String REVIEW_CONTENT = "content";
    public static final String TRAILER_NAME = "name";
    public static final String TRAILER_KEY = "key";


    public static ArrayList<Movie> parseMovies(String jsonResponse) throws JSONException {
        ArrayList<Movie> movies = new ArrayList<>();

        JSONObject movieData = new JSONObject(jsonResponse);

        JSONArray movieJSONArray = movieData.getJSONArray(MOVIE_RESULTS);

        for(int i = 0; i < movieJSONArray.length(); i++) {
            JSONObject movieJSON = movieJSONArray.getJSONObject(i);
            int id = movieJSON.getInt(ID);
            String title = movieJSON.getString(TITLE);
            String releaseDate =  movieJSON.getString(RELEASE_DATE);
            String imgSrc = movieJSON.getString(POSTER_IMAGE_PATH);
            String voteAvg = movieJSON.getString(VOTE_AVG);
            String plotSynopsis = movieJSON.getString(PLOT_SYNOPSIS);

            movies.add(new Movie(id, title, releaseDate, imgSrc, voteAvg, plotSynopsis));
        }

        return movies;
    }

    public static ArrayList<Trailer> parseTrailers(String jsonResponse) throws JSONException {
        ArrayList<Trailer> trailers = new ArrayList<Trailer>();

        JSONObject trailerData = new JSONObject(jsonResponse);

        JSONArray trailerJSONArray = trailerData.getJSONArray(MOVIE_RESULTS);

        for(int i = 0; i < trailerJSONArray.length() && i < 3; i++) {
            JSONObject trailerJSON = trailerJSONArray.getJSONObject(i);
            String name = trailerJSON.getString(TRAILER_NAME);
            String key =  trailerJSON.getString(TRAILER_KEY);

            trailers.add(new Trailer(name, key));
        }

        return trailers;
    }

    public static ArrayList<Review> parseReviews(String jsonResponse) throws JSONException {
        ArrayList<Review> reviews = new ArrayList<Review>();

        JSONObject reviewData = new JSONObject(jsonResponse);

        JSONArray reviewJSONArray = reviewData.getJSONArray(MOVIE_RESULTS);

        for(int i = 0; i < reviewJSONArray.length() && i < 3; i++) {
            JSONObject reviewJSON = reviewJSONArray.getJSONObject(i);
            String author = reviewJSON.getString(REVIEW_AUTHOR);
            String content =  reviewJSON.getString(REVIEW_CONTENT);

            reviews.add(new Review(author, content));
        }

        return reviews;
    }
}
